package com.example.jbarnes.tps900_demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jbarnes.tps900_demo.magnetic.MagneticActivity;
import com.example.jbarnes.tps900_demo.printer.PrinterActivity;
import com.telpo.tps550.api.printer.UsbThermalPrinter;

import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HandshakeCompletedEvent;


public class MainActivity extends Activity {
    EditText trackData1;
    EditText trackData2;
    EditText trackData3;
    Button btnPrint;
    MagneticActivity cardReader;
    Handler handler;
    PrinterActivity printer;
    TimerTask timertask;
    Timer timer;
    final Handler _handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            btnPrint = (Button) findViewById(R.id.btn_print);
            trackData1 = (EditText) findViewById(R.id.TrackNumber1);
            trackData2 = (EditText) findViewById(R.id.TrackNumber2);
            trackData3 = (EditText) findViewById(R.id.TrackNumber3);

            btnPrint.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    printer = new PrinterActivity(MainActivity.this);

                    EditText grayScale = (EditText) findViewById(R.id.grayScale);

                    printer.printContent = createPrintMessage();
                    try {
                        if (!grayScale.getText().equals("")) {

                            int grayscale = Integer.parseInt(grayScale.getText().toString());

                            if (grayscale > 0 && grayscale <= 7) {

                                printer.printGray = grayscale;
                            }
                        }
                    } catch (Exception ex) {
                    }

                    printer.handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            switch (msg.what) {
                                case 3:
                                    break;

                                case 9:
                                    printer.readThread.start();
                                    break;

                                default:

                                    break;
                            }

                        }
                    };

                    printer.start();
                }
            });

        } catch (Exception _ex) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        cardReader = new MagneticActivity(MainActivity.this);

        cardReader.handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                trackData1.setText("");
                trackData2.setText("");
                trackData3.setText("");

                String[] TrackData = (String[]) msg.obj;
                for (int i = 0; i < 3; i++) {
                    if (TrackData[i] != null) {
                        switch (i) {
                            case 0:
                                trackData1.setText(TrackData[i]);
                                break;
                            case 1:
                                trackData2.setText(TrackData[i]);
                                break;
                            case 2:
                                trackData3.setText(TrackData[i]);
                                break;
                        }
                    }
                }

                timer = new Timer();

                timertask = new TimerTask() {

                    public void run() {

                        _handler.post(new Runnable() {

                            public void run() {
                                trackData1.setText("TrackData1");
                                trackData2.setText("TrackData2");
                                trackData3.setText("TrackData3");

                                timer.cancel();
                            }
                        });
                    }

                };
                timer.schedule(timertask, 10000l);

            }
        };

        cardReader.start();
        cardReader.readThread.start();
    }

    public String createPrintMessage() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Prueba \n");
        stringBuilder.append("\n-------------------- \n");
        stringBuilder.append("TrackData 1: " + trackData1.getText() + "\n");
        stringBuilder.append("TrackData 2: " + trackData2.getText() + "\n");
        stringBuilder.append("TrackData 3: " + trackData3.getText() + "\n");

        return stringBuilder.toString();

    }

}
