package com.example.jbarnes.tps900_demo.magnetic;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.magnetic.MagneticCard;

/**
 * Created by J.Barnes on 27/04/17.
 */

public class MagneticActivity{
    public Handler handler;
    public Thread readThread;
    private Activity context;

    public MagneticActivity(Activity Context) {
        this.context=Context;
    }

    public boolean start() {
        try {
            MagneticCard.open(this.context);

        } catch (Exception e) {
            MagneticCard.close();

        }

        readThread = new ReadThread();


        return true;
    }


    private class ReadThread extends Thread {
        String[] TrackData = null;

        @Override
        public void run() {
            MagneticCard.startReading();
            while (!Thread.interrupted()) {
                try {
                    TrackData = MagneticCard.check(1000);
                    handler.sendMessage(handler.obtainMessage(1, TrackData));
                    MagneticCard.startReading();
                } catch (TelpoException e) {

                }
            }
        }

    }


}
