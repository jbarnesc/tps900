package com.example.jbarnes.tps900_demo.printer;

import android.app.Activity;
import android.os.Handler;

import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.printer.UsbThermalPrinter;

/**
 * Created by J.Barnes on 27/04/17.
 */

public class PrinterActivity {
    public Handler handler;
    public Thread readThread;
    private Activity context;
    private int leftDistance;
    private int lineDistance;
    private int wordFont;
    public int printGray;
    public String printContent;
    private int walkPaper;
    UsbThermalPrinter usbThermalPrinter;
    private boolean nopaper;

    public PrinterActivity(Activity Context) {
        this.context = Context;

        this.leftDistance = 0;
        this.lineDistance = 0;
        this.wordFont = 2;
        this.printGray = 7;
        this.printContent = "Prueba";
        this.walkPaper = 20;


    }

    public boolean start() {
        try {
            usbThermalPrinter = new UsbThermalPrinter(this.context);

        } catch (Exception e) {


        }

        readThread = new ReadThread();

        handler.sendMessage(handler.obtainMessage(9, 1, 0, null));

        return true;
    }


    private class ReadThread extends Thread {

        @Override
        public void run() {
            super.run();
            try {

                usbThermalPrinter.reset();
                usbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_LEFT);
                usbThermalPrinter.setLeftIndent(leftDistance);
                usbThermalPrinter.setLineSpace(lineDistance);
                if (wordFont == 4) {
                    usbThermalPrinter.setFontSize(2);
                    usbThermalPrinter.enlargeFontSize(2, 2);
                } else if (wordFont == 3) {
                    usbThermalPrinter.setFontSize(1);
                    usbThermalPrinter.enlargeFontSize(2, 2);
                } else if (wordFont == 2) {
                    usbThermalPrinter.setFontSize(2);
                } else if (wordFont == 1) {
                    usbThermalPrinter.setFontSize(1);
                }
                usbThermalPrinter.setGray(printGray);
                usbThermalPrinter.addString(printContent);
                usbThermalPrinter.printString();
                usbThermalPrinter.walkPaper(walkPaper);
            } catch (Exception e) {
                e.printStackTrace();
                String Result = e.toString();
                if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                    nopaper = true;
                } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                    handler.sendMessage(handler.obtainMessage(12, 1, 0, null));
                } else {
                    handler.sendMessage(handler.obtainMessage(11, 1, 0, null));
                }
            } finally {
                handler.sendMessage(handler.obtainMessage(10, 1, 0, null));
                if (nopaper) {
                    handler.sendMessage(handler.obtainMessage(3, 1, 0, null));
                    nopaper = false;
                    return;
                }
            }
        }
    }


}
